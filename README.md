# gitlab-cli-reports


## About

gitlab-cli-reports is a command line tool for generating timesheet reports from GitLab data.


## Usage

See usage information via the following command:

```sh
gitlab-cli-reports --help
```

Example usage:

```sh
gitlab-cli-reports \
  --host-url https://gitlab.example.com \
  --access-token 123isnotarealtoken \
  --filter-by-date-begin 2000-01-01 \
  --filter-by-author john.doe \
  --filter-by-project-membership
```


## Developing

### Setup

Ensure you have the following dependencies installed.

* python-3.5
* pipenv

Then run the following:

```sh
pipenv install --dev
```


### Running

Use `pipenv` to run the script, like so:

```sh
pipenv run python3 main.py --help
```


### Building

> Note: You made need to install additional dependencies for building (such as the `python3.5-dev` package on debian-based systems). If something is missing, the script will let you know.

From the root of the repo, run the following:

```sh
./build.sh
```

The resulting binary can then be found at `./dist/gitlab-cli-reports`.
