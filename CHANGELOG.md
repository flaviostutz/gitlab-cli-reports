# gitlab-cli-reports Changelog

## 1.1.1 (2020-04-28)

- Fix a bug that caused problematic issue entries to result in an exception, rather than being skipped


## 1.1.0 (2019-02-07)

- Add `--filter-by-project-membership` flag to make usage on large instances with many users (such as gitlab.com) viable.
- Add support for `/remove_time_spent`.
- Fix bad dependencies in Pipfile.


## 1.0.0 (2018-08-21)

Initial release
